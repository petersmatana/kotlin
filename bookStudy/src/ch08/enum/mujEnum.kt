package ch08.enum

enum class MujEnum(val moje: String) {
    X("aj unikód"),
    Y("y")
}

fun zkouskaPoprve() {
    println(MujEnum.X.moje)

    val vstup: String = "y"
    when (vstup) {
        in MujEnum.X.moje ->
            println("mam tu x")
        in MujEnum.Y.moje ->
            println("mam tu y")
        else ->
            println("ani jedno")
    }
}

enum class MujEnum2(val retezec: String, val cislo: Int) {
    VEC1("retezec", 1),
    VEC2("retezec xy", 2)
}

fun zkouskaPodruhe() {
    val vstup: Int = 1
    when (vstup) {
        MujEnum2.VEC1.cislo ->
            println("enum mi vraci ${MujEnum2.VEC1.retezec}")
        MujEnum2.VEC2.cislo ->
            println("enum mi vraci ${MujEnum2.VEC2.retezec}")
        else ->
            println("neco jineho")
    }
}

fun main() {
//    zkouskaPoprve()
    zkouskaPodruhe()
}
