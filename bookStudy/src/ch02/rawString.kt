package ch02

fun main() {
    val escaped = "takto eskejpuju slovo v \"uvozovkach\""
    println(escaped)

    val raw = """tady muzu psal uplne "vsechno", ale templejtu muzu vlozit - $escaped"""
    println(raw)

    val multiline = """zacatek
        tak tohle
        je multiline string
    koneccccc"""
    println(multiline)

    /*
    * trim margin odstranuje mezery do te doby nez narazi na |
    * */
    val trimMargin = """toto
        |je asdasd
        nejaky www |    a dalsi
        |text lol
    """.trimMargin()
    println(trimMargin)

    val trimMargin2 = """toto
        ~je asdasd
        nejaky www ~    a dalsi
        ~text~lol
    """.trimMargin("~")
    println(trimMargin2)
}
