package ch02

import kotlin.test.assertEquals

/*
* kotlin ma:
* - equals() jako structural equality
* - == jako referential equality
* - === je to stejne jako v Jave ==
* */

fun main() {
    assertEquals("ahoj".equals("Ahoj"), false)
    assertEquals("Ahoj".equals("Ahoj"), true)
    assertEquals("Ahoj".equals(null), false)

    assertEquals("hi" == "hi", true)
    assertEquals("hi" == "Hi", false)
    assertEquals("hi" == null, false)
    assertEquals(null == null, true)

    val s1 = "hi"
    val s2 = "hi"
    val s3 = "Hi"

    assertEquals(s1 === s2, true)
    assertEquals(s1 === s3, false)
}
