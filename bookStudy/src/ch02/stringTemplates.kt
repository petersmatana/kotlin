package ch02

fun main() {
    stringTemplate()
}

fun stringTemplate() {
    val price = 13.42
    val tax = 0.08

    println("Castka je $price a s dani je to ${price * (1 + tax)}")
}
