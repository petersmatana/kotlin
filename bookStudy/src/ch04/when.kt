package ch04

fun main() {
    var cislo = 3
    when (cislo) {
        in 1..3 -> {
            println("nejaka lambda")
            print("vstup = $cislo")
        }
        in 2..10 -> {
            print("splni se obe vetve? - ne")
        }
    }
}
