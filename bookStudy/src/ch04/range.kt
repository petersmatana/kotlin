package ch04

val oneToTen: IntRange = 1..10

val aToZ: CharRange = 'a'..'z'

val mujrange: ClosedRange<String> = "ab".."de"

fun main() {
    oneToTen.forEach { print(it) }
    aToZ.forEach { print(it) }

    for (i in oneToTen) {
        print(i)
    }

    for (i in oneToTen.reversed()) {
        print(i)
    }

    println()

    for (i in 1 until 30 step 3) {
        print("$i ")
    }
}
