package ch07

class SecondaryConstructor(x: Int, y: String) {
    constructor(x: Int, y: Int): this(x.toString(), y.toString()) {
        println("konstruktor ktery prijima 2 Int argumenty: x = $x y = $y")
    }

    constructor(x: String, y: String): this(0, y) {
        println("konstruktor ktery prijima 2 String argumenty: x = $x y = $y")
    }

    fun nemuzu() {
//        toto nezkompiluju
//        println("toto jsou dava ktere bych mel dostat z primarniho konstruktoru x = $x y = $y")
    }

    override fun toString(): String {
        return "pretizena metoda toString()"
    }
}

fun main() {
    val sc1 = SecondaryConstructor("a", "b")
    println(sc1.toString())

    println("----")

    val sc2 = SecondaryConstructor(1, 1)
    println(sc2.toString())
}
