package ch07

class Car(
    val yearOfMake: Int,
    val color: String
) {
    var mutableColor = color
    set(value) {
        println("set $value")
        field = color
    }

    // toto spadne na vyjkjimce stackoverflow
//    get() {
//        return mutableColor
//    }

    init {
        println("vola se kdyz vytvarim instanci ale neni to konstruktor?")
    }

    init {
        println("po-down vyhodnocovani")
    }

    init {
        println("treti init")
    }
}

fun main() {
    val car = Car(1997, "red")

    // promenna je imutable
//    car.yearOfMake = 2000
//    car.color = "yellow"
    car.mutableColor = "nejaka barva"

    println("mutableColor ${car.mutableColor}")
}
