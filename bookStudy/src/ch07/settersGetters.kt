package ch07

// tride jsem neudelal konstruktor
// takze kotlin udela defaultni konstruktor
// bez parametru
class MojeTrida() {
    var nejakaPromenna: Int = 0

    var promenna: String = ""
    get() = field
    set(value) {
        println("vola se setter nad promennou")
        if (value.isBlank()) {
            throw Exception("nesmi byt prazdne")
        } else {
            field = value
        }
    }
}

fun main() {
    var mt = MojeTrida()
    mt.promenna = "asdasd"
    print(mt.promenna)
}
