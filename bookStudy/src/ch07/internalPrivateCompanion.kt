package ch07

class InternalPrivateCompanion() {
    internal fun internalFun() {
        println("internal fun")
    }

    private fun privateFun() {
        println("private fun")
    }

    companion object {
        val promenna: String = "ahoj"

        fun fce() {
            println("fce in companion object")
        }
    }
}

fun main() {
    // na me se to tvari jako static method ale jsem zmaten,
    // navic to pry opravdu neni static
    InternalPrivateCompanion.fce()

    InternalPrivateCompanion.Companion

    // toto muzu udelat ale uz tuhle fci neuvidi nikdo mimo modul
    InternalPrivateCompanion().internalFun()

    // nelze
//    InternalPrivateCompanion.privateFun()
}
