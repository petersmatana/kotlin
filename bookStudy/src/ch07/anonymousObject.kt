package ch07

fun begin() {
    val anonymniObjekt = object {
        lateinit var mnenim: String
        val nemenim: String = "default"

        fun vnitvniFunkce(input: String): String {
            return "$nemenim $input"
        }

    }

    anonymniObjekt.mnenim = "tak to je neco"

    println(anonymniObjekt.mnenim)

    println(anonymniObjekt.toString())

    println(anonymniObjekt.vnitvniFunkce("ahoj"))
}

fun createRunable(): Runnable {
    val runnable = object: Runnable {
        override fun run() {
            println("run fun")
        }
    }

    return runnable
}

object Util {
    fun numberOfProcessors(): Int = Runtime.getRuntime().availableProcessors()
}

fun main() {
    begin()
    createRunable().run()

    println(Util.numberOfProcessors())
}
