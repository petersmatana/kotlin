package ch03

fun sumAll(vararg numbers: Int): Int {
    var result = 0
    // _ mi napovedela idea ale asi to chapu
    numbers.forEach { _ ->
        result += 1
    }
    return result
}

fun main() {
    println(sumAll(1, 2, 3, 4, 5, 6, 7, 8, 9))

    val mujlist = listOf<Int>(1,2,3,4,5)
    sumAll(*mujlist.toIntArray())
}
