package ch03

import kotlin.test.assertEquals

fun inferString() = "string"

fun inferNumber() = 13

// Kotlinove Unit je to stejne jako v jave void
fun returnUnit(): Unit = println("hello world")
fun implicitInferUnit(name: String) = println("hello $name")

// v tomto pripade Kotlin kod inferuje jako lambdu
// ale neinferuje typ
fun spatnaLambda() = { 3 }

// toto ani nezkompiluju
//fun lepsiLambda():Int = { 3 }

fun inferReturnType() {
    println("asd")
}

// musim explicitne uvest return key word
fun explicitReturnType(): String {
    val result = "hello"
    return result.capitalize()
}

fun getCapitalise(text: String) = text.capitalize()

fun begin() {
    // ani to nezkompiluju
//    println(inferString(). * 3)
//    println(inferNumber().length)

    // implicitInferUnit vraci Unit
//    println(implicitInferUnit("smonty").length)
    // objekt Unit ale pretezuje toString()
    println(implicitInferUnit("smonty").toString().length)

    // toto nezkompiluju
//    println("${lambda() * 2}")
}

fun main() {
    begin()
    test()
}

fun f1() = 2
fun f2() = { 2 }
fun f3(i: Int) = { n: Int -> n * i }

fun test() {
    assertEquals(f1(), 2)

    // todo, nevim jak na to napsat test
//    assertSame(f2(), () -> kotlin.Int)
    println("f2() = ${f2()}") // () -> kotlin.Int

    assertEquals(f2()(), 2)

    // todo, opet nevim
    println("f3(2) = ${f3(2)}")

    assertEquals(f3(2)(3), 6)
}
