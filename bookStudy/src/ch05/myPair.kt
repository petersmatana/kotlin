package ch05

fun pair() {
    val (x, y) = Pair(1, 2)
    println("x = $x y = $y")

    val a = Pair("ahoj", "bambule")
    println("prvni = ${a.first}, druhy = ${a.second}")

    a.toList().map { it ->
        it.length.toString() }.map { it ->
        println("delka = $it") }

    a.toString().map { print("$it ") }

    println(mapOf(a.first to x, a.second to y, "asd" to 123))
}

fun mapToExample() {
    val airportCode = listOf("LAX", "SFO", "PDX", "SEA", "PRG", "BRN", "BRU")

    val temperatures = airportCode.map { it ->
        it to (Math.random() * 30)
    }

    temperatures.forEach { println(it) }
}

fun triple() {
    val t = Triple(1,2,3)

    println(mapOf(t.first to "a", t.second to "b"))
}

fun main() {
//    pair()
//    mapToExample()
    triple()
}