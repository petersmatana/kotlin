class Moje {

}

fun main(args: Array<String>) {
    first()
    arrayOfObject()
}

fun arrayOfObject() {
    var poleIntu: Array<Moje> = arrayOf(Moje(), Moje())

    for (item in poleIntu) {
        println(item)
    }
}

fun first() {
    var poleIntu: IntArray = intArrayOf(1, 2, 3)
    poleIntu[0] = 12
    poleIntu[2] = 2

    println("1 = " + poleIntu[0])
    println("2 = " + poleIntu[2])
//    println("2 = " + poleIntu[4])
}
