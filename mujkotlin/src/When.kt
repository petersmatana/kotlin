fun main(args: Array<String>) {
//    firstWhen(3)
    mix(13)
}

fun mix(number: Int) {
    when (number) {
        in 1..10 -> print("x is in the range")
//        in validNumbers -> print("x is valid")
        !in 10..20 -> print("x is outside the range")
        else -> print("none of the above")
    }
}

fun firstWhen(number: Int) {
    when (number) {
        1 -> print("x == 1")
        2 -> print("x == 2")
        else -> {
            print("x is neither 1 nor 2")
        }
    }
}