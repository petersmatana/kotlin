fun main() {
//    begin()
//    moreAdvance()
    keepIndex()
}

fun keepIndex() {
    val structure = arrayOf(1, 2, 3, 4)

    structure.forEachIndexed { index, i ->
        println("index = $index, value = $i")
    }
}

fun moreAdvance() {
    val structure = arrayOf(1, 2, 3, 4)

    // lambdicka
    structure.forEach { item ->
        println("result = $item")
    }
}

fun begin() {
    val structure = arrayOf(1, 2, 3, 4)

    // it je defaultni nazev. jinak se to resi lambdou
    structure.forEach {
        println(it)
    }
}