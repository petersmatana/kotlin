package oop

fun main() {
    var address = Address()
    address.name = "moje name"
    address.street = "moje street"
    address.state = "moje state"

    println(address.name)
    println(address.state)
}

class Address {
    var name: String = ""
//        get() {
//            return this.name
//        }
//        set(value) {
//            this.name = value
//        }

    var street: String = ""
    var state: String = ""
}
