package oop

fun main() {
//    Trida(10)
//    Constructors(10)

//    val t3 = Trida3("asd")
//    println(t3.name)
}

class Rectangle(val height: Int, val width: Int) {
    val isSquare: Boolean
    get() {
        return height == width
    }

    val nejakaPromenna: Int
        get() {
            return nejakaPromenna
        }
}

class Trida3(val name: String) {

}

class Constructors {
    init {
        println("init metoda")
    }

    constructor(number: Int) {
        println("constructor number = ${number}")
    }
}

class Trida2 public constructor(number: Int) {

}

class Trida constructor(number: Int) {

    val tmpNumber = number

    init {
        println("prvni init = ${number}")
        println("tmpNumber = ${tmpNumber}")
    }

    init {
        println("druhy init = ${number}")
        println("tmpNumber = ${tmpNumber}")
    }

}