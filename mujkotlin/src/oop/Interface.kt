package oop

fun main() {
    val mt = MojeTrida()
    mt.bar()
    mt.foo()
}

interface MojeInterface {
    fun bar()

    fun foo() {
        println("ahoj")
    }
}

class MojeTrida : MojeInterface {
    override fun bar() {
        println("bar bar")
    }

    override fun foo() {
        super.foo()
    }
}
