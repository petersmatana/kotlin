package FunctionsLambdas

val sum: (Int, Int) -> Int = {
    x: Int, y: Int -> x + y
}

fun main() {
    println(sum(1,1))
    System.out.println("hello from Java");
}