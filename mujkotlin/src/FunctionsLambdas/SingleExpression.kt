package FunctionsLambdas

fun main() {
    println(getString())
    println(getInt())
}

// single expression protoze se jednoduse
// vejde na jeden radek

fun getString(): String = "toto je string"

fun getInt(): Int = 13
