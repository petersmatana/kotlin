package FunctionsLambdas

fun main() {
    tmp(asd = {
        println("asd")
    })

    tmp(number = 10, text = "rrr", asd = {
        Unit
    })
}

fun tmp(number: Int = 0, text: String = "asd", asd: () -> Unit) {

}
