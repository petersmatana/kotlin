fun main() {
    name(name = "asd", number = 13)
    name()
    name(number = 0)
}

fun name(name: String = "default value", number: Int = 42) {
    println("name = $name, number = $number")
}