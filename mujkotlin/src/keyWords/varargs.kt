package keyWords

fun main() {
//    basicInt(1, 2, 3)
//    basicString("lepsi", "to", "bejt", "nemuze")

    val listOfWords = listOf<String>("a", "b", "c")
    val arrayOfWords = arrayOf<String>("a", "b", "c")

//    toto nefunguje a nevim jak to "rozbalit"
//    basicString(listOfWords)
    basicString(*arrayOfWords)
}

fun basicString(vararg text: String) {
    text.forEach { word ->
        println("word = $word")
    }
}

fun basicInt(vararg numbers: Int) {
    for (number in numbers) {
        println("number = $number")
    }
}
