fun main() {
    firstSwitch(10)
}

fun firstSwitch(x: Int) {
    when (x) {
        in 1..10 -> {
            println("between 1..10")
        }
        11 -> {
            println("11")
        }
        else -> {
            println("else")
        }
    }
}