package basics.nullnull

fun main() {
    basic2()
}

//https://www.baeldung.com/kotlin-null-safety#1-the-let-method

fun basic2() {
    val firstName = "Tom"
    val secondName = "Michael"

    val names: List<String?> = listOf(firstName, null, secondName)

    for (item in names) {
        println(item)
    }
}
