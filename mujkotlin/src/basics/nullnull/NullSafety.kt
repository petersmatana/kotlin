package basics.nullnull

fun main() {
//    basic()
//    safetyString("asd")
//    nonSafetyString("null")
    giveUnit()
}

fun giveUnit(): Unit {
    println("neco jako void v Jave")
}

fun safetyString(text: String?) {
    println(text)
}

fun nonSafetyString(text: String) {
    println(text)
}

fun basic() {
    var a: String = "asd"

    if (a == null) {
        println("a == null")
    } else {
        println("a != null")
    }

    var b: String? = null
    if (b == null) {
        println("b == null")
    } else {
        println("b != null")
    }

    var c: String = "asd"
    println(c)
}
