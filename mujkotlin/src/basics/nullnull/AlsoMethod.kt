package basics.nullnull

fun main() {
    alsoMethod()
}

fun alsoMethod() {
    val names: List<String?> = listOf("firstName", null, "secondName")

    var res = listOf<String?>()
    for (item in names) {
        item?.let {
            res = res.plus(it);
            it
        }?.also{it -> println("non nullable value: $it")}
    }
}
