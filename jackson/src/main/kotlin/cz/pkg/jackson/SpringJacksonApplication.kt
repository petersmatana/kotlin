package cz.pkg.jackson

import cz.pkg.jackson.begin.Begin
import cz.pkg.jackson.json2collection.Convert
import cz.pkg.jackson.partlyUnknown.PartlyUnknown
import cz.pkg.jackson.unknown.Unknown
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringJacksonApplication

fun main(args: Array<String>) {
	runApplication<SpringJacksonApplication>(*args)

	Begin().json2object()
	Begin().json2objectVol2()
	Begin().object2json()

	Convert().json2pojo()
	Convert().json2list()

	PartlyUnknown().json2pojo()
	PartlyUnknown().pojo2json()

	Unknown().unknown()
	Unknown().complicateStructure()
}
