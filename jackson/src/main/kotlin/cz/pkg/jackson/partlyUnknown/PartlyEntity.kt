package cz.pkg.jackson.partlyUnknown

import com.fasterxml.jackson.annotation.JsonAnySetter

data class PartlyEntity(
    var key: String,

    // bez anotace @JsonAnySetter nenamapuju unknow ani list klice
    @JsonAnySetter
    val unknown: Map<Any, Any>
) {
    override fun toString(): String {
        var result: String

        result = "key = ${this.key}"
        this.unknown.forEach{ key,value ->
            result += "key = ${key}, value = ${value}"
        }

        return result
    }
}
