package cz.pkg.jackson.partlyUnknown

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

class PartlyUnknown {

    private val jacksonMapper = ObjectMapper().registerModule(KotlinModule())

    fun json2pojo() {
        val jsonList =
            """{"key": "value", "unknown": {"key": "value"}, "list": [1,2,3,4]}"""

        var mujList: PartlyEntity = jacksonMapper.readValue(jsonList, PartlyEntity::class.java)
        println(mujList)
    }

    fun pojo2json() {
        var wrappedData: Map<String, Int> = HashMap<String, Int>()
        wrappedData = mapOf("a" to 1, "b" to 2, "c" to 3)

        var data: Map<Any, Any> = HashMap<Any, Any>()
        data = mapOf("klic1" to 1, "klic2" to 2, "klic3" to wrappedData)

        var pojo: PartlyEntity = PartlyEntity(key = "klic", unknown = data)
        println("pojo = ${pojo.toString()}")

        val result = jacksonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(pojo)
        println(result)
    }
}
