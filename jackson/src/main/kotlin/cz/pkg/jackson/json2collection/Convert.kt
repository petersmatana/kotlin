package cz.pkg.jackson.json2collection

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

class Convert {

    val jacksonMapper = ObjectMapper().registerModule(KotlinModule())

    fun json2pojo() {
        val jsonList =
            """[{"name": "bezkoder", "age": "26", "messages" : ["hello","becoming zKoder"]},
            {"name":"bezkoder Master","age":30,"messages":["I am Kotlin Master","still learning Kotlin"]}]"""
        val result: List<PersonPOJO> =
            jacksonMapper.readValue(jsonList, object : TypeReference<List<PersonPOJO>>() {})

        result.forEach { item ->
            println(item.toString())
        }
    }

    fun json2list() {
        val jsonStr = """[{"a": "value1", "b": 1}, {"a": "value2", "b":2}]"""
        val result: List<Map<String, String>> =
            jacksonMapper.readValue(jsonStr, object : TypeReference<List<Map<String, String>>>() {})

        result.forEach { item ->
            println(item)
        }
    }
}
