package cz.pkg.jackson.json2collection

data class PersonPOJO(
    val name: String,
    val age: Int,
    val messages: List<String>
) {
    override fun toString(): String {
        var result = "name = ${this.name}, age = ${this.age}"
        this.messages.forEach { item ->
            result += " message = ${item} "
        }
        return result
    }
}

