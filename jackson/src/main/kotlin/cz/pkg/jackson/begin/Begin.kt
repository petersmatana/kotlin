package cz.pkg.jackson.begin

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.registerKotlinModule

class Begin {

    private val mapper = jacksonObjectMapper()
    private val m = ObjectMapper().registerModule(KotlinModule())

    fun json2object() {
        val jsonString = "{\"name\":\"Mahesh\", \"age\":21}"

        val entity: EntityPOJO = mapper.readValue(jsonString, EntityPOJO::class.java)
        println("name = ${entity.name}, age = ${entity.age}")
    }

    fun json2objectVol2() {
        val jsonString = "{\"name\":\"Mahesh\", \"age\":21}"

        val m = ObjectMapper().registerModule(KotlinModule())
        val entity: EntityPOJO = m.readValue(jsonString, object : TypeReference<EntityPOJO>(){})
        println("name = ${entity.name}, age = ${entity.age}")
    }

    fun object2json() {
        val entity: EntityPOJO = EntityPOJO(
            name = "entity name",
            age = 13
        )

        // json na jednom radku
        var json = mapper.writeValueAsString(entity)
        println("json = $json")

        // pekne formatovanej json
        var json2 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(entity)
        println("json2 = $json2")
    }

}
