package cz.pkg.jackson.begin

data class EntityPOJO(
    val name: String,
    val age: Int
)