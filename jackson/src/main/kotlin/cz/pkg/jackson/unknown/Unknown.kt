package cz.pkg.jackson.unknown

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule

class Unknown {

    private val jacksonMapper = ObjectMapper().registerModule(KotlinModule())

    fun unknown() {
        val unknownJson = """{"key": "value", "key2": ["item1", "item2", "item3"], "key3": {"key": 13}, "intList": [1,2,3,4,5,6,7,8], "struct":{"a":{"b":{"c":{"end":"exit"}}}, "nejake": "dalsi"} }"""

        val result : Map<String, Any> = jacksonMapper.readValue(
            unknownJson, object : TypeReference<Map<String, Any>>() {})

        result.forEach { key, value ->
            println("key = (${key}), value = (${value})")
        }
    }

    fun complicateStructure() {
        val unknownJson = """{"key": "value", "key2": ["item1", "item2", "item3"], "key3": {"key": 13}}"""

        val result : Map<Any, Any> = jacksonMapper.readValue(
            unknownJson, object : TypeReference<Map<Any, Any>>() {})

        result.forEach { key, value ->
            println("key = (${key}), value = (${value})")
        }
    }

}