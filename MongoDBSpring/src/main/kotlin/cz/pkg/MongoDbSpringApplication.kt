package cz.pkg

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MongoDbSpringApplication

fun main(args: Array<String>) {
	runApplication<MongoDbSpringApplication>(*args)
}
