package cz.pkg.begin.config

import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.data.mongodb.MongoDbFactory
import org.springframework.data.mongodb.MongoTransactionManager
import org.springframework.data.transaction.ChainedTransactionManager
//import org.springframework.orm.jpa.JpaTransactionManager
import org.springframework.transaction.PlatformTransactionManager


@Configuration
class conf {

    // source: https://www.youtube.com/watch?v=qOfdE-cFzto

    @Bean("mongoTransactional")
    fun transactionManagerMongo(dbFactory: MongoDbFactory): MongoTransactionManager {
        return MongoTransactionManager(dbFactory)
    }

//    @Bean("jpaTransactional")
//    fun transactionManagerjpa(): JpaTransactionManager {
//        return JpaTransactionManager()
//    }

    @Bean("mongoJpaTransaction")
    fun chainedTransactionManager(
        @Qualifier("mongoTransactional") mongo: MongoTransactionManager
//        @Qualifier("jpaTransactional") jpa: JpaTransactionManager
    ): PlatformTransactionManager {
//        return ChainedTransactionManager(mongo, jpa)
        return ChainedTransactionManager(mongo)
    }
}
