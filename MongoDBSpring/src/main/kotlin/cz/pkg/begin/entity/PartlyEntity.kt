package cz.pkg.begin.entity

import com.fasterxml.jackson.annotation.JsonAnySetter
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document
data class PartlyEntity(
    @Id
    var id: String? = null,

    var key: String,

    // bez anotace @JsonAnySetter nenamapuju unknow ani list klice
    @JsonAnySetter
    val unknown: Map<Any, Any>
) {
    override fun toString(): String {
        var result: String = "id = ${id}\n"
        result += "key = ${this.key}\n"
        this.unknown.forEach{ key,value ->
            result += "key = ${key}, value = ${value}\n"
        }
        return result
    }
}
