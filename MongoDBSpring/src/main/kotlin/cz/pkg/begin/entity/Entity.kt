package cz.pkg.begin.entity

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document(collection = "mojeTrida")
data class Trida(
    @Id
    val id: String? = null,
    val text: String,
    val cislo: Int,
    val datum: Instant
) {
    override fun toString(): String {
        return "id = ${id}, text = ${text}, cislo = ${cislo}, datum = ${datum}"
    }
}
