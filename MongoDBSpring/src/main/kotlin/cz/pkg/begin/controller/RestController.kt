package cz.pkg.begin.controller

import cz.pkg.begin.service.Service
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class RestController(
    private val service: Service
) {

    @GetMapping("/read")
    fun readData() {
        service.readData()
    }

    @GetMapping("/create")
    fun createData() {
        service.createData()
    }

    @GetMapping("/mongoTemplate")
    fun mongoTemplate() {
        service.mongoTemplate()
    }

    @GetMapping("/saveUnknown")
    fun saveUnknown() {
        service.saveUnknown()
    }

    @GetMapping("/findUnknown")
    fun findUnknown() {
        service.findUnknown()
    }

    @GetMapping("/findPartly")
    fun findPartly() {
        service.findPartly()
    }

    @GetMapping("/deletePartly")
    fun deletePartly() {
        service.deletePartly()
    }

}
