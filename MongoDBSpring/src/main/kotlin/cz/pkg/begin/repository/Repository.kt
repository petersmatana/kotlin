package cz.pkg.begin.repository

import cz.pkg.begin.entity.Trida
import org.springframework.data.mongodb.repository.MongoRepository

interface TridaRepository: MongoRepository<Trida, String>
