package cz.pkg.begin.repository

import cz.pkg.begin.entity.PartlyEntity
import org.springframework.data.mongodb.repository.MongoRepository

interface PartlyRepository : MongoRepository<PartlyEntity, String> {
    fun findPartlyEntityById(id: String): PartlyEntity
    fun deletePartlyEntitiesById(id: String)
}
