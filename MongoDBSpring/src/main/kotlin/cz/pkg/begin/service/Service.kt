package cz.pkg.begin.service

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.mongodb.BasicDBObject
import com.mongodb.DBObject
import com.mongodb.client.MongoClients
import cz.pkg.begin.entity.Trida
import cz.pkg.begin.entity.PartlyEntity
import cz.pkg.begin.repository.TridaRepository
import cz.pkg.begin.repository.PartlyRepository
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.MongoOperations
import org.springframework.data.mongodb.core.MongoTemplate
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.query.Query
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.time.Instant

@Service
class Service(
    private val repository: TridaRepository,
    private val partlyRepository: PartlyRepository
) {

    private val jacksonMapper = ObjectMapper().registerModule(KotlinModule())

    fun readData() {
        val data = repository.findAll()

        data.forEach { item ->
            println("data = ${item.toString()}")
        }
    }

    fun createData() {
        val trida = Trida(
            text = "nejaky text",
            cislo = 13,
            datum = Instant.now()
        )

        repository.insert(trida)
    }

    @Transactional(transactionManager = "mongoJpaTransaction")
    fun mongoTemplate() {
        val json = """{"key": "value", "unknown": {"key": "value"}, "list": [1,2,3,4],"unknown2":["item1","item2","item3"],"intList":[1,2,3,4,5,6,7,8],"key3":{"key":13},"struct":{"a":{"b":{"c":{"end":"exit"}}},"nejake":"dalsi"}}"""
//        val json = """{"key":"value","unknown":["item1","item2","item3"],"intList":[1,2,3,4,5,6,7,8],"key3":{"key":13},"struct":{"a":{"b":{"c":{"end":"exit"}}},"nejake":"dalsi"}}"""

        var mujList: PartlyEntity = jacksonMapper.readValue(json, object : TypeReference<PartlyEntity>() {})
        partlyRepository.save(mujList)

//        var mo: MongoOperations = MongoTemplate(MongoClients.create(), "learn")
//        val persisted = mo.insert(mujList)
//        println("persisted = ${persisted}")
    }

    fun saveUnknown() {
        // https://mongodb.github.io/mongo-java-driver/3.6/javadoc/index.html?com/mongodb/util/JSON.html

        val json = """{"key": "value", "unknown": {"key": "value"}, "list": [1,2,3,4],"unknown2":["item1","item2","item3"],"intList":[1,2,3,4,5,6,7,8],"key3":{"key":13},"struct":{"a":{"b":{"c":{"end":"exit"}}},"nejake":"dalsi"}}"""
        val x = BasicDBObject.parse(json)

        var mo: MongoOperations = MongoTemplate(MongoClients.create(), "learn")
        mo.insert(x)

//        throw RuntimeException("testing transaction")
    }

    fun findUnknown() {
        val query = BasicDBObject("_id", ObjectId("5e89ded3f1b5b1654b3a77ac"))
        println(query)
    }

    fun findPartly() {
        val data = partlyRepository.findPartlyEntityById("5e89ded3f1b5b1654b3a77ac")

        val result = jacksonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(data)
        println(result)
    }

    fun deletePartly() {
        partlyRepository.deletePartlyEntitiesById("5e89d6b36c2afa76ac98e217")
    }

}