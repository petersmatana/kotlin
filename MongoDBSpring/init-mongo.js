db.createUser(
    {
        user: "learnUser",
        pwd: "learnPasswd",
        roles: [
            {
                role: "readWrite",
                db: "topitedev"
            }
        ]
    }
)