fun sayHello() {
    println("hello smonty")
}

/* diky te vlastnosti ze je if vyraz tak nemusim uvadet
{} a tim padem se tomuto zapisu rika "expression body"
pry je toto oblibeny zapis v kotlinu
* */
fun max2(a: Int, b: Int): Int = if (a > b) a else b

/*tento zapis kde jsou {} se oznacuje tak ze tato fce
ma "block body". to protoze existuje skoro ekvivalentni
zapsi
* */
fun max1(a: Int, b: Int): Int {
    /*if v kotlinu neni statement ale vyraz. tzn vyraz
    jako v kazdym jazyce vratim
    * */
    return if (a > b) a else b
}

fun mutableVar() {
    var retezec: String
    retezec = "ahoj"
    println(retezec)

    retezec = "mutace"
    println(retezec)
}

fun immutableVar() {
    // neco jako final v Java
    val retezec: String
    retezec = "cau"
    println(retezec)

//    dokonce idea protestuje :)
//    retezec = "ne?"
}

/*intellij mi napovida abych ocividnou
podminku if (array.size > 0) refaktoroval
na: Option.isFindNotEmpty(array.size)
blby je ze musim neco importovat, to
pada a zatim netusim o co jde
* */
fun takeArgs(array: Array<String>) = if (array.size > 0) printArgs(array) else printNic("nic tu neni")

fun printArgs(array: Array<String>) {
    for (item in array) println(item)
}

fun printNic(test: String) {
    println(test)
}

fun main(args: Array<String>) {
//    sayHello()

//    println("max1 = " + max1(1, 3))
//    println("max1 = " + max2(1, 3))

//    mutableVar()
//    immutableVar()

//    val someArgs: Array<String> = arrayOf("asd", "foo", "bar")
//    takeArgs(arrayOf("asd", "foo", "bar"))
}
